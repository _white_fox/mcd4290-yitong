//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
     
    
    //Question 1 here 
    let myArray=[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19,
-51, -17, -25]
    let positiveOdd=[]
    let nagetiveEven=[]
     for(i= 0;i<myArray.length;i++){
      if (myArray[i] > 0 && myArray[i]%2!=0){
        positiveOdd.push(myArray[i])
       }
      else if (myArray[i]<0 && myArray[i]%2==0)
      {
        nagetiveEven.push(myArray[i])
      }
         
    }  
    output=positiveOdd+"\n"+nagetiveEven;

    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    let dice=0
let face = {one : 0, 
            two : 0,
            three : 0,
            four : 0,
            five : 0,
            six:0  };
for (i=0;i<60000;i++){
  dice =Math.floor((Math.random() * 6) + 1)
  if (dice == 1){
    face.one++;
  }
  else if (dice == 2){
    face.two++;
  }
  else if (dice == 3){
    face.three++;
  }
  else if (dice == 4){
    face.four++;
  }
  else if (dice == 5){
    face.five++;
  }
  else if (dice == 6){
    face.six++;
  }
}
    output ="frequency of the dice rolls"
        +"\n 1:"+face.one
        +"\n 2:"+face.two
        +"\n 3:"+face.three
        +"\n 4:"+face.four
        +"\n 5:"+face.five
        +"\n 6:"+face.six
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    let dice=0
let face = [0,0,0,0,0,0,0]
for (i=0;i<60000;i++){
  dice =Math.floor((Math.random() * 6) + 1)
  face[dice]++
}

output = "frequency of the dice rolls"+"\n 1:"+face[1]
+ "\n 2:"+face[2]
+ "\n 3:"+face[3]
+ "\n 4:"+face[4]
+ "\n 5:"+face[5]
+ "\n 6:"+face[6]

    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
  let dice=0
let Exceptions =[]
var dieRolls ={
  Frequencies:{
    1:0,
    2:0,
    3:0,
    4:0,
    5:0,
    6:0,
  },
  Total:60000,
}
for (i=0;i<60000;i++){
  dice =Math.floor((Math.random() * 6) + 1)
  dieRolls.Frequencies[dice]++
}
for(i=1;i<7;i++){
  if (dieRolls.Frequencies[i] > 10100){
    Exceptions.push(i)
  }
  else if (dieRolls.Frequencies[i] < 9900){
    Exceptions.push(i)
  }
}
console.log("frequency of the dice rolls")
console.log("Total rolls: "+dieRolls.Total)
for(i=1;i<7;i++){
  console.log(i+":"+dieRolls.Frequencies[i])
}
output="frequency of the dice rolls Total rolls: \n"+dieRolls.Total
   + "\n1:"+dieRolls.Frequencies[1]
   + "\n 2:"+dieRolls.Frequencies[2]
   + "\n 3:"+dieRolls.Frequencies[3]
   + "\n 4:"+dieRolls.Frequencies[4]
   + "\n 5:"+dieRolls.Frequencies[5]
   + "\n 6:"+dieRolls.Frequencies[6]+"\n"
   +Exceptions
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    let person = {
 name: "Jane",
 income: 127050
}

if (person.income < 18200){
  output=(" you dont have to pay tax")
}
else if (person.income >18200 && person.income <= 37000){
  output=(person.name+"'s income is: "+ person.income +" and her tax owed is :$"+(person.income*0.19).toFixed(2))
}
else if (person.income >37001 && person.income <= 90000){
  output=(person.name+"'s income is: "+ person.income +" and her tax owed is :$"+((person.income-37000)*0.325+3572).toFixed(2))
}
else if (person.income >90001 && person.income <= 180000){
  output=(person.name+"'s income is: "+ person.income +" and her tax owed is :$"+((person.income-90000)*0.37+20797).toFixed(2))
}
else if (person.income >180001){
  output=(person.name+"'s income is: "+ person.income +" and her tax owed is :$"+((person.income-180000)*0.455+54097).toFixed(2))
}//Question 5 here 
 
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}